all: build push
version := $(shell cat version.txt)
build:
	docker build -t registry.gitlab.com/unixlabs/pocdriver:${version} .
push:
	docker push registry.gitlab.com/unixlabs/pocdriver:${version}
