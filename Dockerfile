FROM maven:3-openjdk-11 as build
RUN set -x \
  && mkdir /build \
  && cd /build \
  && git clone https://github.com/johnlpage/POCDriver.git . \
  && mvn clean package
FROM openjdk:11
COPY --from=build /build/bin/POCDriver.jar /srv/POCDriver.jar
WORKDIR /srv
